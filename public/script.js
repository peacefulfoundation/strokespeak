// Define the characters and reverse characters for display
const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const reverseCharacters = characters.split("").reverse().join("");

// Initialize variables
let currentIndex = 0; // Index of the current character being displayed
let buffer = ""; // Buffer to store characters
let timeoutId; // ID for the timeout used to control character display
let keyPressedTime = 0; // Time when the key was pressed
let lastKeyPressed = ""; // Last key pressed
let spacePressedCount = 0; // Count of consecutive spacebar presses
let anyKeyPressedCount = 0; // Count of consecutive key presses
let isBackward = false; // Flag to indicate if characters should be displayed in reverse

// Get references to HTML elements
const characterDisplay = document.getElementById("characterDisplay");
const bufferDisplay = document.getElementById("bufferDisplay");

// Function to display characters on the screen
function displayCharacter() {
  // Check if the index exceeds the length of the characters array
  if (currentIndex >= characters.length) {
    currentIndex = 0; // Reset the index to restart the loop
  }
  // Display the current character on the screen, taking into account the direction
  characterDisplay.textContent = isBackward
    ? reverseCharacters[currentIndex]
    : characters[currentIndex];
  currentIndex++; // Increment the index for the next character
  // Set a timeout to display the next character after 1.5 seconds
  timeoutId = setTimeout(displayCharacter, 1500);
}

// Function to add a character to the buffer
function addToBuffer(char) {
  buffer += char; // Append the character to the buffer
  bufferDisplay.textContent = buffer; // Update the buffer display on the screen
}

document.getElementById("addButton").addEventListener("click", function () {
  addToBuffer(
    isBackward
      ? reverseCharacters[currentIndex - 1]
      : characters[currentIndex - 1]
  );
});

// Function to delete the last character from the buffer
function deleteLastCharacterFromBuffer() {
  buffer = buffer.slice(0, -1); // Remove the last character from the buffer
  bufferDisplay.textContent = buffer; // Update the buffer display on the screen
}

// Function to speak out the contents of the buffer
function speakBuffer() {
  const utterance = new SpeechSynthesisUtterance(buffer); // Create a new utterance with the buffer content
  speechSynthesis.speak(utterance); // Speak out the contents of the utterance
}
document.getElementById("speakButton").addEventListener("click", speakBuffer);

// Function to clear the buffer
function clearBuffer() {
  buffer = ""; // Clear the buffer
  bufferDisplay.textContent = buffer; // Update the buffer display on the screen
}

document.getElementById("clearButton").addEventListener("click", clearBuffer);

// restart the character loop from the beginning
function restartCharacterLoop() {
  clearTimeout(timeoutId);
  currentIndex = 0;
  displayCharacter();
}

document
  .getElementById("restartButton")
  .addEventListener("click", restartCharacterLoop);

// Event handler for keydown events
function handleKeyDown(event) {
  const currentTime = new Date().getTime(); // Get the current time

  // Check if the spacebar was pressed
  if (event.key === " ") {
    // Check the time difference between consecutive spacebar presses
    if (currentTime - keyPressedTime < 300) {
      spacePressedCount++; // Increment the space pressed count
    } else {
      spacePressedCount = 1; // Reset the space pressed count
    }
    keyPressedTime = currentTime; // Update the time of the last spacebar press

    // Perform actions based on the number of consecutive spacebar presses
    if (spacePressedCount === 1) {
      // Add the current or reversed character to the buffer
      addToBuffer(
        isBackward
          ? reverseCharacters[currentIndex - 1]
          : characters[currentIndex - 1]
      );
    } else if (spacePressedCount === 2) {
      // Delete the last character from the buffer
      deleteLastCharacterFromBuffer();
    } else if (spacePressedCount === 3) {
      // Speak out the contents of the buffer
      speakBuffer();
    } else if (spacePressedCount > 3) {
      // Clear the buffer if more than three consecutive spacebar presses occur
      clearBuffer();
      spacePressedCount = 0; // Reset the space pressed count
    }
  } else {
    // Handle non-spacebar key presses

    // Check if the current key is the same as the last key pressed
    if (event.key === lastKeyPressed) {
      anyKeyPressedCount++; // Increment the count of consecutive key presses
    } else {
      lastKeyPressed = event.key; // Update the last key pressed
      anyKeyPressedCount = 1; // Reset the count of consecutive key presses
    }

    // Perform actions based on the count of consecutive key presses
    if (anyKeyPressedCount === 1) {
      // Restart the character loop from the beginning
      clearTimeout(timeoutId);
      currentIndex = 0;
      displayCharacter();
    } else if (anyKeyPressedCount === 2) {
      // Add a space character to the buffer
      addToBuffer(" ");
    } else if (anyKeyPressedCount === 3) {
      // Reverse the direction of character display and restart the loop
      isBackward = !isBackward;
      currentIndex = isBackward ? characters.length - 1 : 0;
      displayCharacter();
    }
    spacePressedCount = 0; // Reset the space pressed count
  }
}

// Add event listener for keydown events
document.addEventListener("keydown", handleKeyDown);

// Start displaying characters
displayCharacter();

////////////////////////////////////////////////////
////////////////////////////////////////////////////
//
//
// WebAudio Speech Recognition Code.
// This is where all the Voice detection magic works.
//
//

let audioContext;
let sourceNode;
let analyserNode;
let ambientVolume = 0;
let volumeThreshold = 0;
const ambientDuration = 1000;

const recognition = new (window.SpeechRecognition ||
  window.webkitSpeechRecognition)();
recognition.lang = "en-US";
recognition.interimResults = false;
recognition.maxAlternatives = 1;

let isAudioProcessing = false;

function toggleAudioProcessing() {
  if (isAudioProcessing) {
    stopAudioProcessing();
    document.getElementById("toggleButton").innerText =
      " ▶️️Start Voice Detection";
  } else {
    startAudioProcessing();

    document.getElementById("toggleButton").innerText =
      "⏹️Stop Voice Detection";
  }
  isAudioProcessing = !isAudioProcessing;
}

function startAudioProcessing() {
  audioContext = new (window.AudioContext || window.webkitAudioContext)();
  analyserNode = audioContext.createAnalyser();
  analyserNode.fftSize = 2048;
  analyserNode.minDecibels = -90;
  analyserNode.maxDecibels = -10;
  analyserNode.smoothingTimeConstant = 0.85;

  navigator.mediaDevices
    .getUserMedia({ audio: true })
    .then((stream) => {
      sourceNode = audioContext.createMediaStreamSource(stream);
      sourceNode.connect(analyserNode);

      measureAmbientVolume().then((volume) => {
        ambientVolume = volume;
        volumeThreshold = ambientVolume * 1.5;
        console.log("Ambient volume:", ambientVolume);
        console.log("Volume threshold:", volumeThreshold);

        analyzeVolume();
      });
    })
    .catch((error) => {
      console.error("Error accessing microphone:", error);
    });
}

function stopAudioProcessing() {
  if (sourceNode) {
    sourceNode.disconnect();
    sourceNode = null;
  }
  if (analyserNode) {
    analyserNode.disconnect();
    analyserNode = null;
  }
  if (audioContext) {
    audioContext.close();
    audioContext = null;
  }
  if (recognition.state === "active") {
    recognition.stop();
  }
}

function measureAmbientVolume() {
  return new Promise((resolve) => {
    let sum = 0;
    let count = 0;

    function measure() {
      if (!analyserNode) {
        console.error("AnalyserNode is not defined.");
        resolve(0);
        return;
      }

      const bufferLength = analyserNode.frequencyBinCount;
      const dataArray = new Uint8Array(bufferLength);
      analyserNode.getByteFrequencyData(dataArray);

      const rmsVolume = getRMSVolume(dataArray);
      sum += rmsVolume;
      count++;

      if (count < ambientDuration / 100) {
        setTimeout(measure, 100);
      } else {
        resolve(sum / count);
      }
    }

    measure();
  });
}
let lowVolumeThreshold = 0.002;
let mediumVolumeThreshold = 0.004;
let highVolumeThreshold = 0.006;

const lowPitchSlider = document.getElementById("lowPitchSlider");
const lowPitchValue = document.getElementById("lowPitchValue");
const mediumPitchSlider = document.getElementById("mediumPitchSlider");
const mediumPitchValue = document.getElementById("mediumPitchValue");
const highPitchSlider = document.getElementById("highPitchSlider");
const highPitchValue = document.getElementById("highPitchValue");

function setupTresholdValues() {
  lowPitchValue.textContent = lowVolumeThreshold;
  mediumPitchValue.textContent = mediumVolumeThreshold;
  highPitchValue.textContent = highVolumeThreshold;
}

setupTresholdValues();

lowPitchSlider.addEventListener("input", function () {
  console.log(parseFloat(this.value));
  if (parseFloat(this.value) < parseFloat(mediumVolumeThreshold) === false) {
    alert("Low pitch threshold should be less than medium pitch threshold");
    this.value = parseFloat(lowVolumeThreshold);
    return;
  }
  lowVolumeThreshold = parseFloat(this.value);
  lowPitchValue.textContent = lowVolumeThreshold;
  console.log({ lowVolumeThreshold });
});

mediumPitchSlider.addEventListener("input", function () {
  if (parseFloat(this.value) < parseFloat(lowVolumeThreshold) === true) {
    alert("Medium pitch threshold should be greater than low pitch threshold");
    this.value = parseFloat(mediumVolumeThreshold);
    return;
  }
  if (parseFloat(this.value) > parseFloat(highVolumeThreshold) === true) {
    alert("Medium pitch threshold should be less than high pitch threshold");
    this.value = parseFloat(mediumVolumeThreshold);
    return;
  }
  mediumVolumeThreshold = parseFloat(this.value);
  mediumPitchValue.textContent = mediumVolumeThreshold;
  console.log({ mediumVolumeThreshold });
});

highPitchSlider.addEventListener("input", function () {
  const value = parseFloat(this.value);
  console.log({ value });
  console.log({ mediumVolumeThreshold });
  console.log(value < mediumVolumeThreshold);
  if (value < parseFloat(mediumVolumeThreshold)) {
    alert("High pitch threshold should be greater than medium pitch threshold");
    this.value = parseFloat(highVolumeThreshold);
    return;
  }
  highVolumeThreshold = value;
  highPitchValue.textContent = highVolumeThreshold;
  console.log({ highVolumeThreshold });
});

function analyzeVolume() {
  if (!analyserNode) {
    console.error("AnalyserNode is not defined.");
    return;
  }

  const bufferLength = analyserNode.frequencyBinCount;
  const dataArray = new Uint8Array(bufferLength);

  let isCharacterAdded = false; // Flag to track if a character has been added

  function update() {
    if (!analyserNode) {
      console.error("AnalyserNode is not available.");
      return;
    }
    analyserNode.getByteFrequencyData(dataArray);

    const rmsVolume = getRMSVolume(dataArray);
    console.log("Current volume:", rmsVolume);

    if (rmsVolume > volumeThreshold) {
      const frequency = getFrequency(dataArray);
      console.log({ frequency });

      console.log({ rmsVolume });
      document.getElementById("rmsVolume").innerText = rmsVolume;

      const isLowerPitch =
        parseFloat(rmsVolume) <= parseFloat(lowVolumeThreshold);

      const isMediumPitch =
        parseFloat(rmsVolume) <= parseFloat(mediumVolumeThreshold) &&
        parseFloat(rmsVolume) > parseFloat(lowVolumeThreshold);

      const isHigherPitch =
        parseFloat(rmsVolume) > parseFloat(mediumVolumeThreshold) &&
        parseFloat(rmsVolume) <= parseFloat(highVolumeThreshold);

      // check for rms
      if (isLowerPitch) {
        console.log("Low pitch detected!");

        // restart all the magic
        // restartCharacterLoop();

        document.getElementById("low-volume-message").style.display = "block";
        document.getElementById("high-volume-message").style.display = "none";
        document.getElementById("medium-volume-message").style.display = "none";
      } else if (isMediumPitch) {
        console.log("Medium pitch detected!");
        document.getElementById("low-volume-message").style.display = "none";
        document.getElementById("high-volume-message").style.display = "none";
        document.getElementById("medium-volume-message").style.display =
          "block";
      } else if (isHigherPitch) {
        console.log("High pitch detected!");
        document.getElementById("high-volume-message").style.display = "block";
        document.getElementById("low-volume-message").style.display = "none";
        document.getElementById("medium-volume-message").style.display = "none";
      }

      if (!isCharacterAdded) {
        addToBuffer(
          isBackward
            ? reverseCharacters[currentIndex - 1]
            : characters[currentIndex - 1]
        );

        isCharacterAdded = true; // Set the flag to true
      }

      //   startSpeechRecognition();
    } else {
      console.log("No voice is detected!");
      document.getElementById("low-volume-message").style.display = "none";
      document.getElementById("high-volume-message").style.display = "none";
      document.getElementById("medium-volume-message").style.display = "none";
      document.getElementById("rmsVolume").innerText = 0;
      isCharacterAdded = false; // Reset the flag
    }

    if (recognition.state !== "active") {
      requestAnimationFrame(update);
    }
  }

  update();
}

function getRMSVolume(dataArray) {
  let sum = 0;
  for (let i = 0; i < dataArray.length; i++) {
    sum += dataArray[i] * dataArray[i];
  }
  const rms = Math.sqrt(sum / dataArray.length);
  return rms / 255;
}

function getFrequency(dataArray) {
  const maxMagnitude = Math.max(...dataArray);
  const maxIndex = dataArray.indexOf(maxMagnitude);
  const frequency = maxIndex * (audioContext.sampleRate / analyserNode.fftSize);
  return frequency;
}
// function startSpeechRecognition() {
//   recognition.start();
// }

recognition.onresult = function (event) {
  const result = event.results[0][0].transcript;
  console.log("Recognized speech:", result);
};

recognition.onerror = function (event) {
  console.error("Speech recognition error:", event.error);
};

recognition.onend = function () {
  console.log("Speech recognition ended.");
  analyzeVolume();
};

document
  .getElementById("toggleButton")
  .addEventListener("click", toggleAudioProcessing);
