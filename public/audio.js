let audioContext;
let sourceNode;
let analyserNode;
let ambientVolume = 0;
let volumeThreshold = 0;
const ambientDuration = 3000;

const recognition = new (window.SpeechRecognition ||
  window.webkitSpeechRecognition)();
recognition.lang = "en-US";
recognition.interimResults = false;
recognition.maxAlternatives = 1;

function startAudioProcessing() {
  audioContext = new (window.AudioContext || window.webkitAudioContext)();
  analyserNode = audioContext.createAnalyser();
  analyserNode.fftSize = 2048;
  analyserNode.minDecibels = -90;
  analyserNode.maxDecibels = -10;
  analyserNode.smoothingTimeConstant = 0.85;

  navigator.mediaDevices
    .getUserMedia({ audio: true })
    .then((stream) => {
      sourceNode = audioContext.createMediaStreamSource(stream);
      sourceNode.connect(analyserNode);

      measureAmbientVolume().then((volume) => {
        ambientVolume = volume;
        volumeThreshold = ambientVolume * 1.5;
        console.log("Ambient volume:", ambientVolume);
        console.log("Volume threshold:", volumeThreshold);

        analyzeVolume();
      });
    })
    .catch((error) => {
      console.error("Error accessing microphone:", error);
    });
}

function measureAmbientVolume() {
  return new Promise((resolve) => {
    let sum = 0;
    let count = 0;

    function measure() {
      if (!analyserNode) {
        console.error("AnalyserNode is not defined.");
        resolve(0);
        return;
      }

      const bufferLength = analyserNode.frequencyBinCount;
      const dataArray = new Uint8Array(bufferLength);
      analyserNode.getByteFrequencyData(dataArray);

      const rmsVolume = getRMSVolume(dataArray);
      sum += rmsVolume;
      count++;

      if (count < ambientDuration / 100) {
        setTimeout(measure, 100);
      } else {
        resolve(sum / count);
      }
    }

    measure();
  });
}

function analyzeVolume() {
  if (!analyserNode) {
    console.error("AnalyserNode is not defined.");
    return;
  }

  const bufferLength = analyserNode.frequencyBinCount;
  const dataArray = new Uint8Array(bufferLength);

  function update() {
    analyserNode.getByteFrequencyData(dataArray);

    const rmsVolume = getRMSVolume(dataArray);
    console.log("Current volume:", rmsVolume);

    if (rmsVolume > volumeThreshold) {
      console.log("High volume detected!");
      const icon = document.getElementById("high-volume-message");
      icon.style.display = "block";

      //   startSpeechRecognition();
    } else {
      console.log("Low volume detected!");
      document.body.style.backgroundColor = "white"; // Change background color to white

      const icon = document.getElementById("high-volume-message");
      icon.style.display = "none";
    }

    if (recognition.state !== "active") {
      requestAnimationFrame(update);
    }
  }

  update();
}

function getRMSVolume(dataArray) {
  let sum = 0;
  for (let i = 0; i < dataArray.length; i++) {
    sum += dataArray[i] * dataArray[i];
  }
  const rms = Math.sqrt(sum / dataArray.length);
  return rms / 255;
}

// function startSpeechRecognition() {
//   recognition.start();
// }

recognition.onresult = function (event) {
  const result = event.results[0][0].transcript;
  console.log("Recognized speech:", result);
};

recognition.onerror = function (event) {
  console.error("Speech recognition error:", event.error);
};

recognition.onend = function () {
  console.log("Speech recognition ended.");
  analyzeVolume();
};

document
  .getElementById("toggleButton")
  .addEventListener("click", startAudioProcessing);
